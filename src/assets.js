// build.sh replaces values below by actual content.

class DefaultData {
	static POLL = `{{ DEFAULT_DATA_POLL }}`;
	static CONFIG = `{{ DEFAULT_DATA_CONFIG }}`;
	static STYLE = `{{ DEFAULT_DATA_STYLE }}`;
}

class PollSample {
	static MOVIES = `{{ POLL_SAMPLE_MOVIES }}`;
	static DETAILED_MOVIES = `{{ POLL_SAMPLE_DETAILED_MOVIES }}`;
}

class Templates {
	static MERIT_PROFILE_SVG = `{{ MERIT_PROFILE_SVG }}`;
	static MERIT_PROFILE_HTML = `{{ MERIT_PROFILE_OG }}`;
}
