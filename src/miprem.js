function rgb_linear(alpha, color_start, color_end) {
	const r1 = parseInt(color_start.slice(1, 3), 16);
	const g1 = parseInt(color_start.slice(3, 5), 16);
	const b1 = parseInt(color_start.slice(5, 7), 16);
	const r2 = parseInt(color_end.slice(1, 3), 16);
	const g2 = parseInt(color_end.slice(3, 5), 16);
	const b2 = parseInt(color_end.slice(5, 7), 16);
	const r = Math.round(r1 * (1 - alpha) + alpha * r2);
	const g = Math.round(g1 * (1 - alpha) + alpha * g2);
	const b = Math.round(b1 * (1 - alpha) + alpha * b2);
	return '#' + r.toString(16).padStart(2, "0") + g.toString(16).padStart(2, "0")
		+ b.toString(16).padStart(2, "0");
}

function get_color_id(nb_colors, id_grade, nb_grades) {
	alpha = id_grade / (nb_grades - 1);
	return alpha * (nb_colors - 1);
}

class Renderer {

	constructor(template, config, style) {
		config = Object.assign({}, DefaultData.CONFIG, config);
		style = DefaultData.STYLE + style;

		this.template = template;
		this.config = config;
		this.style = style;

		this.env = nunjucks.configure({ autoescape: true });
		this.env.addFilter('grade_color', function(n_grade, nb_grades) {
			const color_palette = config.grades_color_palette;
			const color_id = get_color_id(color_palette.length, n_grade, nb_grades)

			const dec_part = color_id - Math.floor(color_id);
			const start_col = color_palette[Math.floor(color_id)];
			const end_col = color_palette[Math.ceil(color_id)];

			return dec_part == 0 ? start_col : rgb_linear(dec_part, start_col, end_col);
		});
	}

	check_tally(tally) {
		const sums = []
		for(const proposal_tally of tally) {
			sums.push(proposal_tally.reduce((a, b) => a + b, 0));
		}
		const total = sums.reduce((a, b) => a + b, 0);
		for(const sum of sums) {
			if (sum != total / sums.length) {
				return "Proposal tallies sum are not equals: " + sums.join(', ') + "."
			}
		}
		return "";
	}

	render_svg(raw_poll, callback) {
		const poll = Object.assign({}, DefaultData.POLL, raw_poll);
		callback(this.env.renderString(this.template, {
			poll: poll,
			config: this.config,
			style: this.style,
			error: this.check_tally(poll.tally)
		}));
	}

	render_png() {
		// TBC
	}

	render_og(raw_poll, callback) {
// <meta property="og:title" content="{{ poll.question.label }}" />
// <meta property="og:image" content="{{ poll.merit_profile_png_url }}" />
// <meta property="og:image:secure_url" content="{{ poll.secure_url }}" />
// <meta property="og:image:type" content="image/svg+xml" />
// <meta property="og:image:width" content="{{ config.width }}" />
// <meta property="og:image:height" content="{{ config.height }}" />
// <meta property="og:image:alt" content="The merite profile image of the poll &quote;{{ poll.question.label }}&quote;." />
// <meta property="og:description" content="The merite profile image of the poll &quote;{{ poll.question.label }}&quote;." />
// <meta property="og:locale" content="en" />
// <meta property="og:site_name" content="Miprem" />


	// min image size: 600x315 (see https://developers.facebook.com/docs/sharing/webmasters/images)
	// use og:locale when i18n will be used in poll data (ie. question/label-en)
	}
}
