const cm_theme = 'solarized dark';
const gid = id => document.getElementById(id);
const mp_svg_template = Templates.MERIT_PROFILE_SVG;
const poll_config = {'width': 680, 'height': 300};

let poll = {'text': '', 'mode': 'yaml', 'tab-id': 'poll-tab'};
let config = {'text': '', 'mode': 'yaml', 'tab-id': 'config-tab'};
let style = {'text': '', 'mode': 'css', 'tab-id': 'style-tab'};

let active_code_block = {};
let code_mirror;

function load_codeblocks_data() {
	active_code_block['text'] = code_mirror.getValue();

	try {
		poll_data = jsyaml.load(poll['text']);
	} catch (error) {
		console.log('Poll: invalid yaml:\n' + poll['text'] +
			'\nWaiting for valid poll values to upload figure...');
		return;
	}

	try {
		config_data = jsyaml.load(config['text']);
	} catch (error) {
		console.log('Config: invalid yaml:\n' + config['text'] +
			'\nWaiting for valid config values to upload figure...');
		return;
	}
}

function render_svg() {
	load_codeblocks_data();
	renderer = new Renderer(mp_svg_template, config_data, style['text']);
	renderer.render_svg(poll_data, svg => gid('miprem-svg').innerHTML = svg);
	tippy('.svg-tooltip');
}

window.addEventListener('load', function () {
	poll['text'] = jsyaml.dump(Object.assign({}, DefaultData.POLL, PollSample.MOVIES))
	config['text'] = jsyaml.dump(Object.assign({}, DefaultData.CONFIG, poll_config));
	style['text'] = DefaultData.STYLE;

	code_mirror = CodeMirror(gid('code-block'), {
		value: poll['text'],
		mode:  "yaml",
		theme: cm_theme
	});
	code_mirror.on('change', () => render_svg());

	[poll, config, style].forEach((code_block, i) => add_tab_event(code_block));
	render_svg();
})

function add_tab_event(code_block) {
	gid(code_block['tab-id']).addEventListener('click', function () {
		active_code_block = code_block;
		Array.from(gid('tabs').children).forEach((e, i) => e.classList.remove('active'));
		gid(code_block['tab-id']).parentNode.classList.add('active');
		code_mirror.setValue(code_block['text'].trim());
		code_mirror.setOption('mode', code_block['mode']);
	});
}
